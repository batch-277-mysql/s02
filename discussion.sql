--  List down all the databases inside the DBMS

SHOW DATABASES;


--     CREATE a database
--     Syntax:
--     CREATE DATABASE <database_name>;


CREATE DATABASE music_db;

--     DROP a database, delete a database
--     Syntax:
--     DROP DATABASE <database_name>;


DROP DATABASE music_db;


--     USE a database or select a database
--     Syntax:
--     USE <database_name>;

USE music_db;

--     CREATE a table
-- Table columns will also be declared here
--     Syntax:
--     CREATE TABLE <table_name> (
--         <column_name> <data_type> <constraints>,
--         <column_name> <data_type> <constraints>,
--     );

CREATE TABLE users (
--      INT to indicate that the id column wll have integer values
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number CHAR(11) NOT NULL,
    email VARCHAR(50) NOT NULL,
    address VARCHAR(50) NULL ,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
--      PRIMARY KEY to indicate that the id column is the primary key
    PRIMARY KEY (id)
);


-- Drop table from database

DROP TABLE userss;


-- Create table for playlist

CREATE TABLE playlist (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    playlist_name varchar(30)  NOT NULL,
    datatime_created DATETIME NOT NULL,
    primary key (id),
    CONSTRAINT fk_playlist_user_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);


-- Create table for artists

CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    artist_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);


-- Mini Activity

-- Create table for albums

CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_album_artist_id
        FOREIGN KEY (artist_id)
        REFERENCES artists(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

-- Mini Activity

-- Create table for songs(id, song_name, length,genre, album_id)

CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_song_album_id
        FOREIGN KEY (album_id)
        REFERENCES albums(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);


-- Mini Activity

-- Create table for playlist_songs

CREATE TABLE playlist_songs (
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlist_songs_playlist_id
        FOREIGN KEY (playlist_id)
        REFERENCES playlist(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
    CONSTRAINT fk_playlist_songs_song_id
        FOREIGN KEY (song_id)
        REFERENCES songs(id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);